-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Май 01 2021 г., 08:17
-- Версия сервера: 10.4.16-MariaDB
-- Версия PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `team2db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add content type', 4, 'add_contenttype'),
(14, 'Can change content type', 4, 'change_contenttype'),
(15, 'Can delete content type', 4, 'delete_contenttype'),
(16, 'Can view content type', 4, 'view_contenttype'),
(17, 'Can add session', 5, 'add_session'),
(18, 'Can change session', 5, 'change_session'),
(19, 'Can delete session', 5, 'delete_session'),
(20, 'Can view session', 5, 'view_session'),
(21, 'Can add user', 6, 'add_person'),
(22, 'Can change user', 6, 'change_person'),
(23, 'Can delete user', 6, 'delete_person'),
(24, 'Can view user', 6, 'view_person'),
(25, 'Can add Category', 7, 'add_category'),
(26, 'Can change Category', 7, 'change_category'),
(27, 'Can delete Category', 7, 'delete_category'),
(28, 'Can view Category', 7, 'view_category'),
(29, 'Can add city', 8, 'add_city'),
(30, 'Can change city', 8, 'change_city'),
(31, 'Can delete city', 8, 'delete_city'),
(32, 'Can view city', 8, 'view_city'),
(33, 'Can add company', 9, 'add_company'),
(34, 'Can change company', 9, 'change_company'),
(35, 'Can delete company', 9, 'delete_company'),
(36, 'Can view company', 9, 'view_company'),
(37, 'Can add region', 10, 'add_region'),
(38, 'Can change region', 10, 'change_region'),
(39, 'Can delete region', 10, 'delete_region'),
(40, 'Can view region', 10, 'view_region'),
(41, 'Can add role', 11, 'add_role'),
(42, 'Can change role', 11, 'change_role'),
(43, 'Can delete role', 11, 'delete_role'),
(44, 'Can view role', 11, 'view_role'),
(45, 'Can add user', 12, 'add_user'),
(46, 'Can change user', 12, 'change_user'),
(47, 'Can delete user', 12, 'delete_user'),
(48, 'Can view user', 12, 'view_user'),
(49, 'Can add transport', 13, 'add_transport'),
(50, 'Can change transport', 13, 'change_transport'),
(51, 'Can delete transport', 13, 'delete_transport'),
(52, 'Can view transport', 13, 'view_transport'),
(53, 'Can add manager', 14, 'add_manager'),
(54, 'Can change manager', 14, 'change_manager'),
(55, 'Can delete manager', 14, 'delete_manager'),
(56, 'Can view manager', 14, 'view_manager'),
(57, 'Can add hotel', 15, 'add_hotel'),
(58, 'Can change hotel', 15, 'change_hotel'),
(59, 'Can delete hotel', 15, 'delete_hotel'),
(60, 'Can view hotel', 15, 'view_hotel'),
(61, 'Can add event', 16, 'add_event'),
(62, 'Can change event', 16, 'change_event'),
(63, 'Can delete event', 16, 'delete_event'),
(64, 'Can view event', 16, 'view_event'),
(65, 'Can add rank', 17, 'add_rank'),
(66, 'Can change rank', 17, 'change_rank'),
(67, 'Can delete rank', 17, 'delete_rank'),
(68, 'Can view rank', 17, 'view_rank');

-- --------------------------------------------------------

--
-- Структура таблицы `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2021-04-30 10:32:34.520851', '2', 'kamoliddin', 1, '[{\"added\": {}}]', 6, 1),
(2, '2021-04-30 18:02:33.369440', '1', 'AirBNB', 1, '[{\"added\": {}}]', 9, 1),
(3, '2021-04-30 18:04:10.067786', '2', 'Oasis Travel', 1, '[{\"added\": {}}]', 9, 1),
(4, '2021-04-30 18:05:44.255286', '3', 'katherine', 1, '[{\"added\": {}}]', 6, 1),
(5, '2021-04-30 18:06:46.263611', '4', 'sarvar', 1, '[{\"added\": {}}]', 6, 1),
(6, '2021-04-30 18:07:11.372541', '3', 'Hot Spot Travel Agency', 1, '[{\"added\": {}}]', 9, 1),
(7, '2021-04-30 18:23:44.311576', '1', 'Master class', 1, '[{\"added\": {}}]', 7, 1),
(8, '2021-04-30 18:23:52.308615', '2', 'workshops', 1, '[{\"added\": {}}]', 7, 1),
(9, '2021-04-30 18:24:19.074647', '3', 'conferences', 1, '[{\"added\": {}}]', 7, 1),
(10, '2021-04-30 18:24:26.217301', '4', 'seminar', 1, '[{\"added\": {}}]', 7, 1),
(11, '2021-04-30 18:37:30.866079', '1', 'HolidayTurn', 1, '[{\"added\": {}}]', 15, 1),
(12, '2021-04-30 19:21:54.791772', '2', 'Burj Al Arab', 1, '[{\"added\": {}}]', 15, 1),
(13, '2021-04-30 19:30:25.122508', '1', 'Honda Civic', 1, '[{\"added\": {}}]', 13, 1),
(14, '2021-04-30 19:32:54.887835', '2', 'Mercedes-Benz E-class', 1, '[{\"added\": {}}]', 13, 1),
(15, '2021-04-30 19:35:01.033772', '3', 'Porsche', 1, '[{\"added\": {}}]', 13, 1),
(16, '2021-05-01 04:35:14.701872', '1', 'Master Chef', 1, '[{\"added\": {}}]', 16, 1),
(17, '2021-05-01 04:41:01.446618', '2', 'Technology Conference', 1, '[{\"added\": {}}]', 16, 1),
(18, '2021-05-01 04:42:48.532715', '3', 'The beauty of a Women', 1, '[{\"added\": {}}]', 16, 1),
(19, '2021-05-01 05:03:08.700085', '1', 'Rank object (1)', 1, '[{\"added\": {}}]', 17, 1),
(20, '2021-05-01 05:03:16.952209', '2', 'Rank object (2)', 1, '[{\"added\": {}}]', 17, 1),
(21, '2021-05-01 05:03:23.062358', '3', 'Rank object (3)', 1, '[{\"added\": {}}]', 17, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'contenttypes', 'contenttype'),
(5, 'sessions', 'session'),
(7, 'travel', 'category'),
(8, 'travel', 'city'),
(9, 'travel', 'company'),
(16, 'travel', 'event'),
(15, 'travel', 'hotel'),
(14, 'travel', 'manager'),
(6, 'travel', 'person'),
(17, 'travel', 'rank'),
(10, 'travel', 'region'),
(11, 'travel', 'role'),
(13, 'travel', 'transport'),
(12, 'travel', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-04-30 10:26:50.822903'),
(2, 'contenttypes', '0002_remove_content_type_name', '2021-04-30 10:26:50.854962'),
(3, 'auth', '0001_initial', '2021-04-30 10:26:51.051764'),
(4, 'auth', '0002_alter_permission_name_max_length', '2021-04-30 10:26:51.107263'),
(5, 'auth', '0003_alter_user_email_max_length', '2021-04-30 10:26:51.127543'),
(6, 'auth', '0004_alter_user_username_opts', '2021-04-30 10:26:51.141131'),
(7, 'auth', '0005_alter_user_last_login_null', '2021-04-30 10:26:51.150784'),
(8, 'auth', '0006_require_contenttypes_0002', '2021-04-30 10:26:51.153404'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2021-04-30 10:26:51.161749'),
(10, 'auth', '0008_alter_user_username_max_length', '2021-04-30 10:26:51.170426'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2021-04-30 10:26:51.216530'),
(12, 'auth', '0010_alter_group_name_max_length', '2021-04-30 10:26:51.233225'),
(13, 'auth', '0011_update_proxy_permissions', '2021-04-30 10:26:51.244028'),
(14, 'auth', '0012_alter_user_first_name_max_length', '2021-04-30 10:26:51.251412'),
(15, 'travel', '0001_initial', '2021-04-30 10:26:52.499879'),
(16, 'admin', '0001_initial', '2021-04-30 10:26:52.624653'),
(17, 'admin', '0002_logentry_remove_auto_add', '2021-04-30 10:26:52.649172'),
(18, 'admin', '0003_logentry_add_action_flag_choices', '2021-04-30 10:26:52.664143'),
(19, 'sessions', '0001_initial', '2021-04-30 10:26:52.691088'),
(20, 'travel', '0002_auto_20210430_1031', '2021-04-30 10:31:42.776442'),
(21, 'travel', '0003_company_manager', '2021-04-30 10:34:32.284738'),
(22, 'travel', '0004_auto_20210430_1800', '2021-04-30 18:00:19.636026'),
(23, 'travel', '0005_auto_20210430_1802', '2021-04-30 18:02:17.615244'),
(24, 'travel', '0006_alter_category_name', '2021-04-30 18:16:12.454503'),
(25, 'travel', '0007_alter_category_name', '2021-04-30 18:23:17.518687');

-- --------------------------------------------------------

--
-- Структура таблицы `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('u63rhc165acczlucvsxooznh1e6r6tz2', '.eJxVjDsOwjAQBe_iGln471DS5wzW7npNAsiW4qRC3B0ipYD2zcx7iQTbOqWt85LmLC5CidPvhkAPrjvId6i3JqnVdZlR7oo8aJdjy_y8Hu7fwQR9-tYFwYKPIWhkAoeDAc_FRs8UlRq8KWS1O7PLjJppUA60IRUKB-RcULw_B1s5Jw:1lcQMp:Y7emDk6_f-7_cE6py_PTWasH-Tbl9nJcVp5LwELYHHQ', '2021-05-14 10:27:27.838026');

-- --------------------------------------------------------

--
-- Структура таблицы `travel_category`
--

CREATE TABLE `travel_category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_category`
--

INSERT INTO `travel_category` (`id`, `name`, `slug`) VALUES
(1, 'Master class', 'master-class'),
(2, 'workshops', 'workshops'),
(3, 'conferences', 'conferences'),
(4, 'seminar', 'seminar');

-- --------------------------------------------------------

--
-- Структура таблицы `travel_city`
--

CREATE TABLE `travel_city` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `region_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_city`
--

INSERT INTO `travel_city` (`id`, `name`, `region_id`) VALUES
(1, 'Marhamat District', 1),
(2, 'Andijan District', 1),
(3, 'Balikchi District', 1),
(4, 'Boz District', 1),
(5, 'Bulakbashi District', 1),
(6, 'Izboskan District', 1),
(7, 'Ulugnor District', 1),
(8, 'Kurgontepa District', 1),
(9, 'Asaka District', 1),
(10, 'Pakhtaabad District', 1),
(11, 'Jalakuduk District', 1),
(12, 'Oltinkol District', 1),
(13, 'Khodjaabad District', 1),
(14, 'Shakhrihon District', 1),
(15, 'Andijan', 1),
(16, 'Khonobod ', 1),
(17, 'Olot District', 2),
(18, 'Bukhara District', 2),
(19, 'Vabkent District', 2),
(20, 'Kogon District', 2),
(21, 'Karakul District', 2),
(22, 'Karavulbazar District', 2),
(23, 'Peshku District', 2),
(24, 'Romitan District', 2),
(25, 'Shofirkon District', 2),
(26, 'Jondor District', 2),
(27, 'Bukhara ', 2),
(28, 'Kogon ', 2),
(29, 'Gijduvon District', 2),
(30, 'Dustlik District', 3),
(31, 'Zaamin District', 3),
(32, 'Zarbdor District', 3),
(33, 'Mirzachul District', 3),
(34, 'Farish District', 3),
(35, 'Arnasay District', 3),
(36, 'Bakhmal District', 3),
(37, 'Gallaorol District', 3),
(38, 'Yangiabad District', 3),
(39, 'Jizzakh District', 3),
(40, 'Pakhtakor District', 3),
(41, 'Zafarobod District', 3),
(42, 'Sharof Rashidov District', 3),
(43, 'Muborak District', 4),
(44, 'Kasbi District', 4),
(45, 'Guzar District', 4),
(46, 'Dekhanabad District', 4),
(47, 'Kamashi District', 4),
(48, 'Karshi', 4),
(49, 'Chirakchi District', 4),
(50, 'Shakhrisabz District', 4),
(51, 'Yakkabog District', 4),
(52, 'Karshi District', 4),
(53, 'Koson District', 4),
(54, 'Kitab District', 4),
(55, 'Mirishkor District', 4),
(56, 'Nishon District', 4),
(57, 'Shakhrisabz', 4),
(58, 'Konimekh District', 5),
(59, 'Kiziltepa District', 5),
(60, 'Navbahor District', 5),
(61, 'Karmana District', 5),
(62, 'Nurota District', 5),
(63, 'Uchkuduk District', 5),
(64, 'Khatirchi District', 5),
(65, 'Navoi District', 5),
(66, 'Tomdi District', 5),
(67, 'Zarafshon ', 5),
(68, 'Mingbulok District', 6),
(69, 'Kosonsoy District', 6),
(70, 'Namangan District', 6),
(71, 'Norin District', 6),
(72, 'Pap District', 6),
(73, 'Turakurgan District', 6),
(74, 'Uychi District', 6),
(75, 'Uchkurgan District', 6),
(76, 'Chortok District', 6),
(77, 'Chust District', 6),
(78, 'Namangan ', 6),
(79, 'Yangikurgan District', 6),
(80, 'Kushrabot District', 7),
(81, 'Samarkand', 7),
(82, 'Ishtikhan District', 7),
(83, 'Kattakurgan District', 7),
(84, 'Akdarya District', 7),
(85, 'Bulungur District', 7),
(86, 'Jambay District', 7),
(87, 'Narpay District', 7),
(88, 'Pastdargam District', 7),
(89, 'Pakhtachi District', 7),
(90, 'Samarkand District', 7),
(91, 'Nurabad District', 7),
(92, 'Urgut District', 7),
(93, 'Taylak District', 7),
(94, 'Payarik District', 7),
(95, 'Kattakurgan District', 7),
(96, 'Oltinsoy District', 8),
(97, 'Angor District', 8),
(98, 'Boysun District', 8),
(99, 'Denov District', 8),
(100, 'Kumkurgan District', 8),
(101, 'Kizirik District', 8),
(102, 'Sariosiyo District', 8),
(103, 'Termez District', 8),
(104, 'Uzun District', 8),
(105, 'Sherabad District', 8),
(106, 'Shurchi District', 8),
(107, 'Termez', 8),
(108, 'Muzrabot District', 8),
(109, 'Jarkurgan District', 8),
(110, 'Mirzaabod District', 9),
(111, 'Syrdarya District', 9),
(112, 'Okoltin District', 9),
(113, 'Boyovut District', 9),
(114, 'Gulistan District', 9),
(115, 'Saykhunabad District', 9),
(116, 'Sardoba District', 9),
(117, 'Shirin ', 9),
(118, 'Yangiyer', 9),
(119, 'Khovos District', 9),
(120, 'Gulistan ', 9),
(121, 'Yunusabad District', 10),
(122, 'Uchtepa District', 10),
(123, 'Bektemir District', 10),
(124, 'Mirzo Ulugbek District', 10),
(125, 'Mirabad District', 10),
(126, 'Olmazor District', 10),
(127, 'Sirgeli District', 10),
(128, 'Yakkasaray District', 10),
(129, 'Chilanzar District', 10),
(130, 'Yashnabad District', 10),
(131, 'Shaykhantahur District', 10),
(132, 'Buka District', 11),
(133, 'Akkurgan District', 11),
(134, 'Akhangaran District', 11),
(135, 'Bekabad District', 11),
(136, 'Bostanlyk District', 11),
(137, 'Zangiota District', 11),
(138, 'Kibray District', 11),
(139, 'Parkent District', 11),
(140, 'Urtachirchik District', 11),
(141, 'Chinaz District', 11),
(142, 'Yangiyul District', 11),
(143, 'Yukorichirchik District', 11),
(144, 'Pskent District', 11),
(145, 'Almalyk ', 11),
(146, 'Angren', 11),
(147, 'Chirchik', 11),
(148, 'Kuyichirchik District', 11),
(149, 'Tashkent District', 11),
(150, 'Nurafshon ', 11),
(151, 'Bekobod', 11),
(152, 'Ohangaron', 11),
(153, 'Yangiyul ', 11),
(154, 'Kuva District', 12),
(155, 'Dangara District', 12),
(156, 'Sukh District', 12),
(157, 'Altiarik District', 12),
(158, 'Kushtepa District', 12),
(159, 'Bagdad District', 12),
(160, 'Buvayda District', 12),
(161, 'Besharik District', 12),
(162, 'Uchkuprik District', 12),
(163, 'Rishton District', 12),
(164, 'Toshlok District', 12),
(165, 'Uzbekistan District', 12),
(166, 'Fergana District', 12),
(167, 'Furkat District', 12),
(168, 'Yozyovon District', 12),
(169, 'Fergana', 12),
(170, 'Kokand', 12),
(171, 'Kuvasoy', 12),
(172, 'Margilan', 12),
(173, 'Hazorasp District', 13),
(174, 'Yangiarik District', 13),
(175, 'Yangibozor District', 13),
(176, 'Khonka District', 13),
(177, 'Khiva District', 13),
(178, 'Urgench ', 13),
(179, 'Shovot District', 13),
(180, 'Bogot District', 13),
(181, 'Gurlan District', 13),
(182, 'Kushkupir District', 13),
(183, 'Urgench District', 13),
(184, 'Khiva', 13),
(185, 'Beruniy District', 14),
(186, 'Karauzak District', 14),
(187, 'Kegeyli District', 14),
(188, 'Kungirat District', 14),
(189, 'Kanlikul District', 14),
(190, 'Muynak District', 14),
(191, 'Shumanay District', 14),
(192, 'Ellikkala District', 14),
(193, 'Nukus', 14),
(194, 'Amudarya District', 14),
(195, 'Nukus District', 14),
(196, 'Takhtakupir District', 14),
(197, 'Turtkul District', 14),
(198, 'Khujayli District', 14),
(199, 'Chimboy District', 14),
(200, 'Takhiatosh District', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_company`
--

CREATE TABLE `travel_company` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) NOT NULL,
  `manager_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_company`
--

INSERT INTO `travel_company` (`id`, `company_name`, `phone_number`, `manager_id`) VALUES
(1, 'AirBNB', '+998909310205', 2),
(2, 'Oasis Travel', '+998973459691', 2),
(3, 'Hot Spot Travel Agency', '+998909598099', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_event`
--

CREATE TABLE `travel_event` (
  `id` bigint(20) NOT NULL,
  `date` datetime(6) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `district` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `event_name` varchar(100) DEFAULT NULL,
  `event_description` varchar(200) DEFAULT NULL,
  `event_phone` varchar(20) DEFAULT NULL,
  `category_id` bigint(20) NOT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_event`
--

INSERT INTO `travel_event` (`id`, `date`, `photo`, `district`, `street`, `event_name`, `event_description`, `event_phone`, `category_id`, `city_id`, `region_id`, `user_id`) VALUES
(1, '2021-05-01 04:35:14.697067', 'secondary/masterchef-logo.jpg', 'Yunusabad', 'Uvaysiy street', 'Master Chef', 'Learn 50 basics with Chef Gordon Ramsay', '+998996549999', 1, 57, 10, 4),
(2, '2021-05-01 04:41:01.438680', 'secondary/technology-conference-poster-template_1361-1297.jpg', 'Angren district', 'Norwood', 'Technology Conference', '11, 12, 13 July. Technology Conference 2020', '+9989746546543', 3, 146, 11, 2),
(3, '2021-05-01 04:42:48.527858', 'secondary/beauty.jpg', 'Yunusabad', '45 Drivewood circle', 'The beauty of a Women', 'World empowerment seminar. Sunday at 10:00 pm', '+998946157513', 4, 16, 10, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_hotel`
--

CREATE TABLE `travel_hotel` (
  `id` bigint(20) NOT NULL,
  `date` datetime(6) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `district` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `hotel_name` varchar(100) DEFAULT NULL,
  `hotel_description` varchar(200) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_hotel`
--

INSERT INTO `travel_hotel` (`id`, `date`, `photo`, `district`, `street`, `hotel_name`, `hotel_description`, `email`, `city_id`, `region_id`, `user_id`) VALUES
(1, '2021-04-30 18:37:30.861873', 'secondary/holidayTurn.jpg', 'Niyoziy', 'Ziyolilar street', 'HolidayTurn', 'TripAdvisor Great boutique hotel, a must stay in there', 'holidayturn@gmail.co', 15, 1, 3),
(2, '2021-04-30 19:21:54.785085', 'secondary/burj.jpg', 'Umm Suqeim 3', 'Um', 'Burj Al Arab', 'Set on an island in a striking sail-shaped building, this luxury hotel', 'burj@gmail.co', 48, 4, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_manager`
--

CREATE TABLE `travel_manager` (
  `id` bigint(20) NOT NULL,
  `manager_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `travel_person`
--

CREATE TABLE `travel_person` (
  `id` bigint(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `age` smallint(5) UNSIGNED NOT NULL CHECK (`age` >= 0),
  `phone_number` varchar(100) NOT NULL,
  `user_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_person`
--

INSERT INTO `travel_person` (`id`, `password`, `last_login`, `is_superuser`, `username`, `is_staff`, `is_active`, `date_joined`, `first_name`, `last_name`, `email`, `age`, `phone_number`, `user_type`) VALUES
(1, 'pbkdf2_sha256$260000$MKoP6zDdGcNNYw9Rm9xnzN$iCX+C4H0DRzvCRdrw2WRVgZMACNOqk9f635N429U8uU=', '2021-04-30 10:27:27.832008', 1, 'admin', 1, 1, '2021-04-30 10:27:25.783814', '', '', 'admin@gmail.co', 0, '', 'SIMPLEUSER'),
(2, 'kamoliddin', NULL, 0, 'kamoliddin', 0, 1, '2021-04-30 10:31:51.000000', 'Kamoliddin', 'Chinaliev', 'chkb007147@gmail.com', 22, '+998946157513', 'MANAGER'),
(3, 'katherine', NULL, 0, 'katherine', 0, 1, '2021-04-30 18:04:23.000000', 'Katherine', 'Smirnova', 'kath@gmail.com', 18, '+99897347707', 'MANAGER'),
(4, 'sarvar', NULL, 0, 'sarvar', 0, 1, '2021-04-30 18:05:44.000000', 'Sarvar', 'Arifdjanov', 'sarvar@gmail.co', 21, '+99897387984', 'SIMPLEUSER');

-- --------------------------------------------------------

--
-- Структура таблицы `travel_person_groups`
--

CREATE TABLE `travel_person_groups` (
  `id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `travel_person_user_permissions`
--

CREATE TABLE `travel_person_user_permissions` (
  `id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `travel_rank`
--

CREATE TABLE `travel_rank` (
  `id` bigint(20) NOT NULL,
  `value` smallint(6) NOT NULL,
  `date` date NOT NULL,
  `event_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_rank`
--

INSERT INTO `travel_rank` (`id`, `value`, `date`, `event_id`) VALUES
(1, 3, '2021-05-01', 1),
(2, 2, '2021-05-01', 2),
(3, 5, '2021-05-01', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_region`
--

CREATE TABLE `travel_region` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_region`
--

INSERT INTO `travel_region` (`id`, `name`, `code`) VALUES
(1, 'Andijan Region', 1),
(2, 'Bukhara Region', 2),
(3, 'Jizzakh Region', 3),
(4, 'Kashkadarya Region', 4),
(5, 'Navoi Region', 5),
(6, 'Namangan Region', 6),
(7, 'Samarkand Region', 7),
(8, 'Surkhandarya Region', 8),
(9, 'Syrdarya Region', 9),
(10, 'Tashkent', 10),
(11, 'Tashkent Region', 11),
(12, 'Fergana Region', 12),
(13, 'Khorezm Region', 13),
(14, 'Republic of Karakalpakstan', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_transport`
--

CREATE TABLE `travel_transport` (
  `id` bigint(20) NOT NULL,
  `date` datetime(6) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `district` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `tran_name` varchar(100) DEFAULT NULL,
  `capacity` double DEFAULT NULL,
  `is_available` tinyint(1) DEFAULT NULL,
  `owner_phone` varchar(20) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `travel_transport`
--

INSERT INTO `travel_transport` (`id`, `date`, `photo`, `district`, `street`, `tran_name`, `capacity`, `is_available`, `owner_phone`, `city_id`, `region_id`, `user_id`) VALUES
(1, '2021-04-30 19:30:25.115122', 'secondary/2022-civic-sport-touring-15-1619201090.jpg', 'Andijan district', '45 Drivewood circle', 'Honda Civic', 2, 1, '+998946157513', 15, 1, 4),
(2, '2021-04-30 19:32:54.886026', 'secondary/2021-mercedes-benz-e-class-103-1583172964.jpg', 'Bukhara district', '55 Drivewood circle', 'Mercedes-Benz E-class', 80, 0, '+99897123465878', 27, 2, 1),
(3, '2021-04-30 19:35:01.031537', 'secondary/2021-porsche-718-cayman-mmp-1-1593003156.jpg', 'Hollywood district', '101 Hollywod circle', 'Porsche', 65, 1, '+9989946531156', 178, 14, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `travel_user`
--

CREATE TABLE `travel_user` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Индексы таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Индексы таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_travel_person_id` (`user_id`);

--
-- Индексы таблицы `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Индексы таблицы `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Индексы таблицы `travel_category`
--
ALTER TABLE `travel_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `travel_city`
--
ALTER TABLE `travel_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_city_region_id_6f0c23f6_fk_travel_region_id` (`region_id`);

--
-- Индексы таблицы `travel_company`
--
ALTER TABLE `travel_company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD KEY `travel_company_manager_id_da6f1a7c_fk_travel_person_id` (`manager_id`);

--
-- Индексы таблицы `travel_event`
--
ALTER TABLE `travel_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_event_category_id_f2ebb6dc_fk_travel_category_id` (`category_id`),
  ADD KEY `travel_event_city_id_bffb52e5_fk_travel_city_id` (`city_id`),
  ADD KEY `travel_event_region_id_d35055af_fk_travel_region_id` (`region_id`),
  ADD KEY `travel_event_user_id_f2a43a38_fk_travel_person_id` (`user_id`);

--
-- Индексы таблицы `travel_hotel`
--
ALTER TABLE `travel_hotel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_hotel_city_id_da13f8b7_fk_travel_city_id` (`city_id`),
  ADD KEY `travel_hotel_region_id_437976ee_fk_travel_region_id` (`region_id`),
  ADD KEY `travel_hotel_user_id_2afe5abc_fk_travel_person_id` (`user_id`);

--
-- Индексы таблицы `travel_manager`
--
ALTER TABLE `travel_manager`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_manager_manager_id_557c5943_fk_travel_person_id` (`manager_id`);

--
-- Индексы таблицы `travel_person`
--
ALTER TABLE `travel_person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Индексы таблицы `travel_person_groups`
--
ALTER TABLE `travel_person_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `travel_person_groups_person_id_group_id_ecf9581e_uniq` (`person_id`,`group_id`),
  ADD KEY `travel_person_groups_group_id_f340d455_fk_auth_group_id` (`group_id`);

--
-- Индексы таблицы `travel_person_user_permissions`
--
ALTER TABLE `travel_person_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `travel_person_user_permi_person_id_permission_id_928ae4e1_uniq` (`person_id`,`permission_id`),
  ADD KEY `travel_person_user_p_permission_id_cf15206b_fk_auth_perm` (`permission_id`);

--
-- Индексы таблицы `travel_rank`
--
ALTER TABLE `travel_rank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_rank_event_id_351a8ddb_fk_travel_event_id` (`event_id`);

--
-- Индексы таблицы `travel_region`
--
ALTER TABLE `travel_region`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `travel_transport`
--
ALTER TABLE `travel_transport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_transport_city_id_f8d0f2e8_fk_travel_city_id` (`city_id`),
  ADD KEY `travel_transport_region_id_47f4fb3e_fk_travel_region_id` (`region_id`),
  ADD KEY `travel_transport_user_id_18058d38_fk_travel_person_id` (`user_id`);

--
-- Индексы таблицы `travel_user`
--
ALTER TABLE `travel_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `travel_user_user_id_a2c32f3d_fk_travel_person_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `travel_category`
--
ALTER TABLE `travel_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `travel_city`
--
ALTER TABLE `travel_city`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT для таблицы `travel_company`
--
ALTER TABLE `travel_company`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `travel_event`
--
ALTER TABLE `travel_event`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `travel_hotel`
--
ALTER TABLE `travel_hotel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `travel_manager`
--
ALTER TABLE `travel_manager`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `travel_person`
--
ALTER TABLE `travel_person`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `travel_person_groups`
--
ALTER TABLE `travel_person_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `travel_person_user_permissions`
--
ALTER TABLE `travel_person_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `travel_rank`
--
ALTER TABLE `travel_rank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `travel_region`
--
ALTER TABLE `travel_region`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `travel_transport`
--
ALTER TABLE `travel_transport`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `travel_user`
--
ALTER TABLE `travel_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_travel_person_id` FOREIGN KEY (`user_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_city`
--
ALTER TABLE `travel_city`
  ADD CONSTRAINT `travel_city_region_id_6f0c23f6_fk_travel_region_id` FOREIGN KEY (`region_id`) REFERENCES `travel_region` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_company`
--
ALTER TABLE `travel_company`
  ADD CONSTRAINT `travel_company_manager_id_da6f1a7c_fk_travel_person_id` FOREIGN KEY (`manager_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_event`
--
ALTER TABLE `travel_event`
  ADD CONSTRAINT `travel_event_category_id_f2ebb6dc_fk_travel_category_id` FOREIGN KEY (`category_id`) REFERENCES `travel_category` (`id`),
  ADD CONSTRAINT `travel_event_city_id_bffb52e5_fk_travel_city_id` FOREIGN KEY (`city_id`) REFERENCES `travel_city` (`id`),
  ADD CONSTRAINT `travel_event_region_id_d35055af_fk_travel_region_id` FOREIGN KEY (`region_id`) REFERENCES `travel_region` (`id`),
  ADD CONSTRAINT `travel_event_user_id_f2a43a38_fk_travel_person_id` FOREIGN KEY (`user_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_hotel`
--
ALTER TABLE `travel_hotel`
  ADD CONSTRAINT `travel_hotel_city_id_da13f8b7_fk_travel_city_id` FOREIGN KEY (`city_id`) REFERENCES `travel_city` (`id`),
  ADD CONSTRAINT `travel_hotel_region_id_437976ee_fk_travel_region_id` FOREIGN KEY (`region_id`) REFERENCES `travel_region` (`id`),
  ADD CONSTRAINT `travel_hotel_user_id_2afe5abc_fk_travel_person_id` FOREIGN KEY (`user_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_manager`
--
ALTER TABLE `travel_manager`
  ADD CONSTRAINT `travel_manager_manager_id_557c5943_fk_travel_person_id` FOREIGN KEY (`manager_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_person_groups`
--
ALTER TABLE `travel_person_groups`
  ADD CONSTRAINT `travel_person_groups_group_id_f340d455_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `travel_person_groups_person_id_15bfb2ed_fk_travel_person_id` FOREIGN KEY (`person_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_person_user_permissions`
--
ALTER TABLE `travel_person_user_permissions`
  ADD CONSTRAINT `travel_person_user_p_permission_id_cf15206b_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `travel_person_user_p_person_id_3cceaa54_fk_travel_pe` FOREIGN KEY (`person_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_rank`
--
ALTER TABLE `travel_rank`
  ADD CONSTRAINT `travel_rank_event_id_351a8ddb_fk_travel_event_id` FOREIGN KEY (`event_id`) REFERENCES `travel_event` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_transport`
--
ALTER TABLE `travel_transport`
  ADD CONSTRAINT `travel_transport_city_id_f8d0f2e8_fk_travel_city_id` FOREIGN KEY (`city_id`) REFERENCES `travel_city` (`id`),
  ADD CONSTRAINT `travel_transport_region_id_47f4fb3e_fk_travel_region_id` FOREIGN KEY (`region_id`) REFERENCES `travel_region` (`id`),
  ADD CONSTRAINT `travel_transport_user_id_18058d38_fk_travel_person_id` FOREIGN KEY (`user_id`) REFERENCES `travel_person` (`id`);

--
-- Ограничения внешнего ключа таблицы `travel_user`
--
ALTER TABLE `travel_user`
  ADD CONSTRAINT `travel_user_user_id_a2c32f3d_fk_travel_person_id` FOREIGN KEY (`user_id`) REFERENCES `travel_person` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
