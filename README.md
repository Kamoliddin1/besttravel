# Database Project

This project is a done with django framework and Python language. Using Mysql as database

## Setup and create database(database inside betTravel folder named team2db.sql)

## Installation
First you should have installed [python](https://www.python.org/).

Go to root

```bash
cd bestTravel
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements.

```bash
pip3 install -r requirements.txt
```

Check them and make sure all requirements are ready.

```bash
pip3 freeze
```

## Usage

## Get ready for migrations
```python
python manage.py makemigrations
```
## Migrate
```python
python manage.py migrate
```
## Create user
```python
python manage.py createsuperuser
```
## Run the web-site and open in localhost:8000 PORT
```python
python manage.py runserver
```

## Contributors
Smirnova Ekaterina U1810014
Arifdjanov Sarvar U1810017
Dadamyants David U1810067
Chinaliev Kamoliddin U1810278

## License
free to use
