from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import (
    Event,Manager, 
    Company, Hotel,
    Region, City,
    Transport, Category, Rank
)
User = get_user_model()

admin.site.register(User)
admin.site.register(Manager)
admin.site.register(Company)
admin.site.register(Event)
admin.site.register(Hotel)
admin.site.register(Region)
admin.site.register(City)
admin.site.register(Transport)
admin.site.register(Category)
admin.site.register(Rank)
