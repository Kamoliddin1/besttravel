from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic import ListView

from .models import Hotel, Transport, Company, Event


class HotelListView(ListView):
    model = Hotel
    # template_name = "travel/index.html"
    queryset = Hotel.objects.all()


class TransportListView(ListView):
    model = Transport
    # template_name = "travel/index.html"
    queryset = Transport.objects.all()


class IndexListView(ListView):

    def get(self, request, *args, **kwargs):
        context = {
            'hotel_list': Hotel.objects.all(),
            'transport_list': Transport.objects.all(),
            'event_list': Event.objects.all(),
            'company_list': Company.objects.all(),
        }
        return render(request, template_name='travel/index.html', context=context)
