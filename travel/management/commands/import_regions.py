import json
import os

from django.core.management.base import BaseCommand, CommandError

from travel.models import Region, City

script_dir = os.path.dirname(__file__)
dist_rel_path = "common_district.json"
reg_rel_path = "common_region.json"

dist_file_path = os.path.join(script_dir, dist_rel_path)
reg_file_path = os.path.join(script_dir, reg_rel_path)


class Command(BaseCommand):
    help = 'Import regions from json file'

    def handle(self, *args, **options):
        self.stdout.write(self.style.HTTP_NOT_MODIFIED('Import in proccess... wait...'))
        try:
            districs_file = open(dist_file_path)
            regions_file = open(reg_file_path)
        except FileNotFoundError:
            raise CommandError("Files doesn\'t exist")

        districs = json.load(districs_file)
        regions = json.load(regions_file)

        for region in regions:
            print(region['name_en'])
            Region.objects.create(
                                  name=region['name_en'],
                                  code=region['id'])
        for dist in districs:
            region = Region.objects.filter(code=dist['region_id']).first()
            City.objects.create(name=dist['name_en'],
                                region=region)
            print(dist['name_en'])

        self.stdout.write(self.style.SUCCESS('Successfully imported'))
