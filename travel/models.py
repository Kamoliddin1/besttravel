from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class Person(AbstractUser):
    class Types(models.TextChoices):
        MANAGER = "MANAGER", "Manager"
        SIMPLEUSER = "SIMPLEUSER", "SimpleUser"

    base_type = Types.SIMPLEUSER

    # What type of user are we?
    user_type = models.CharField(
        _("Type"), max_length=50, choices=Types.choices, default=base_type
    )
    first_name = models.CharField(blank=True, max_length=255)
    last_name = models.CharField(blank=True, max_length=255)
    email = models.EmailField(blank=True)
    age = models.PositiveSmallIntegerField(default=0, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)


class Manager(models.Model):
    manager = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, blank=True)
    base_type = Person.Types.MANAGER

class User(models.Model):
    user = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, blank=True)


class Company(models.Model):
    company_name = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=100, unique=True)
    manager = models.ForeignKey(Person, on_delete=models.CASCADE,blank=True, null=True)

    def __str__(self):
        return self.company_name


class Region(models.Model):
    name = models.CharField(max_length=255)
    code = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=255)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, related_name='districts')

    def __str__(self):
        return self.name


class SecondaryInfo(models.Model):
    date = models.DateTimeField(auto_now=True)
    photo = models.ImageField(upload_to='secondary/')
    district = models.CharField(max_length=100, null=True, blank=True)
    street = models.CharField(max_length=100, null=True, blank=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        abstract = True


class Hotel(SecondaryInfo):
    hotel_name = models.CharField(max_length=100, null=True, blank=True)
    hotel_description = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __str__(self):
        return self.hotel_name


class Transport(SecondaryInfo):
    tran_name = models.CharField(max_length=100, null=True, blank=True)
    capacity = models.FloatField(null=True, blank=True)
    is_available = models.BooleanField(null=True, blank=True)
    owner_phone = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.tran_name


class Category(models.Model):
    name = models.CharField(max_length=100, null=True)
    slug = models.SlugField(max_length=50, unique=True)    

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Event(SecondaryInfo):
    event_name = models.CharField(max_length=100, null=True, blank=True)
    event_description = models.CharField(max_length=200, null=True, blank=True)
    event_phone = models.CharField(max_length=20, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.event_name


class Rank(models.Model):
    value = models.SmallIntegerField(default=0)
    date = models.DateField(auto_now=True)
    event = models.ForeignKey("Event", on_delete=models.CASCADE)