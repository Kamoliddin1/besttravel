from django.urls import path
from .views import HotelListView, IndexListView

urlpatterns = [
    path('', IndexListView.as_view(), name='hotel_list'),
]
